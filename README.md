# Debian dotfiles

## `dwm` - Dynamic Window Manager

![dwm 6.2 screenshot](./screenshots/2020-03-22-dwm.png)

## `dunst` notification daemon

![dunst notification screenshot](./screenshots/2020-03-24-dunst.png)

## `vim`

![vim with dracula theme](./screenshots/2020-03-26-vim.png)

## `tmux`

![tmux old config](./screenshots/2020-04-02-tmux.png)
