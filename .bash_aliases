alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -vI'
alias update='sudo apt update && sudo apt upgrade'
alias tmux='tmux -u -2'
# alias mocp='mocp --theme yellow_red_theme'
# alias news='newsboat'
alias la='exa -al --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias git-log="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias git-simple-log="git log --pretty=oneline --abbrev-commit"
alias xopen='xdg-open'
# alias yt2pm3='youtube-dl -x --audio-format mp3 --prefer-ffmpeg'
